from scpi import SDS
import matplotlib
import matplotlib.pyplot as plt
import csv
host = "192.168.1.12"
port = 5025

# class SDS(scpi.SCPI):

if __name__ == "__main__":
	scope = SDS(host, port, timeout=3)
	scope.printIDN()
	# scope.screenDump('files/scdp.png')
	wfs = scope.getWaveform()


	# save to csv
	class myDialect(csv.Dialect):
		delimiter = ','
		quotechar = '"'
		doublequote = True
		lineterminator = '\n'
		quoting = csv.QUOTE_MINIMAL

	with open('files/wf.csv', 'w') as f:
		writer = csv.writer(f,dialect=myDialect)
		writer.writerow(['time [s]', 'C1 [V]', 'C2 [V]'])
		for i in range(len(wfs.c1.volts)):
			writer.writerow([
				wfs.times[i],wfs.c1.volts[i], wfs.c2.volts[i]
			])
	# plotting...
	matplotlib.rc_file('plotrc.yml')
	plt.style.use('dark_background')
	fig, ax = plt.subplots()
	ax.xaxis.set_major_formatter(matplotlib.ticker.EngFormatter(unit='s'))
	ax.yaxis.set_major_formatter(matplotlib.ticker.EngFormatter(unit='V'))
	try:
		plt.plot(
			wfs.times,
			wfs.c1.volts,
			label= "C1",
			color= "tab:olive"
			)
		plt.plot(
			wfs.times,
			wfs.c2.volts,
			label= "C2",
			color= "tab:pink"
			)
		plt.plot(
			wfs.times,
			wfs.c3.volts,
			label= "C3",
			color= "tab:blue"
			)
		plt.plot(
			wfs.times,
			wfs.c1.volts,
			label= "C4",
			color= "tab:green"
			)
	except AttributeError:
		pass
	plt.legend()
	plt.show()