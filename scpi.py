import socket
import io
import os
import time
from PIL import Image


class SCPI:
	_socket = None
	_buf = 4096
	_timeout = 0.2

	def __init__(self, host:str, port:int=5025, timeout:float=None):

		# init socket
		self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		if timeout:
			self._timeout = timeout
		self._socket.settimeout(self._timeout)

		# try connecting...
		try:
			self._socket.connect((host,port))
			print("Connected to", host, "on port", port)
		except socket.error as e:
			print("Failed to connect to socket :( !")
			print(e)

	def _write(self, cmd:str):
		cmdbinary = cmd.encode()
		self._socket.sendall(cmdbinary)
		self._socket.sendall(b'\n')

	def write(self,cmd:str):
		self._write(cmd)

	def _read(self, bufsize:int=4096, timeout:float=None, end=b'\n' ) -> bytes:
		buffer = bytes()
		if timeout:
			self._socket.settimeout(timeout)
		while True:
			data = self._socket.recv(self._buf)
			if data:
				buffer += data
				if data.endswith(end):
					break
		self._socket.settimeout(self._timeout)
		return buffer

	def query(self, cmd:str) -> bytes:
		self._write(cmd)
		ans = self._read()
		return ans

	def ask(self, cmd:str) -> str:
		"""
		Common query, expecting a string formatted answer
		"""
		self._write(cmd)
		answer = self._read()
		return answer.decode()

	def __del__(self):
		self._socket.close()


class SDS(SCPI):

	class _ChannelData():
		vdiv = 0
		ofst = 0
		volts = []
		vmin = 0
		vmax = 0

	class _WaveForms():
		tdiv = 0
		sara = 0
		times = []
		c1 = None
		c2 = None
		c3 = None
		c4 = None


	def printIDN(self):
		idnStr = self.ask("*IDN?")
		make, model, ser, rev = tuple(idnStr.split(','))
		print("IDN INFO:")
		print("\tMake:\t", make, sep='')
		print("\tModel:\t", model, sep='')
		print("\tS/N:\t", ser, sep='')
		print("\tRev:\t", rev, sep='')

	def screenDump(self, fileName:str=None):
		"""
		Request a screenshot by invoking SCREEN_DUMP | SCDP.

		Returns BMP data which can be written into BMP file.

		Parameters:
			fileName: 	file name to save screenshot to (BMP or PNG)
					if no extension given, defaults to PNG.
		"""
		self._write('SCDP')
		bmpData = self._read()
		if fileName:
			# check given file extension...
			name , dnu , ext = fileName.partition('.')

			if ext == 'bmp':
				tosave = bmpData
			else:
				# Save as PNG.
				ext = 'png'
				b = io.BytesIO(bmpData)
				with Image.open(b) as img:
					pngData = io.BytesIO()
					img.save(pngData,format="PNG")
					tosave = pngData.getvalue()
			# if file with given filename exists, save as filename001, filename002, etc.
			fileSaveName = name + '.' + ext
			fileNO=0
			while os.path.exists(fileSaveName):
				fileNO += 1
				fileSaveName = name + "{:03d}".format(fileNO) + "." + ext
			with open(fileSaveName, mode='wb') as f:
				f.write(tosave)
			print("Saved screenshot to", fileSaveName)

	def getWaveform(self, channels:[int]=[1,2,3,4]):
		print("Acquiring waveform...")
		wfs = self._WaveForms()
		self.write("chdr off")
		tdiv = float(self.query("tdiv?"))
		sara = float(self.query("sara?"))
		wfs.tdiv = tdiv
		wfs.sara = sara
		wfs.times = [ i/sara for i in range(int(tdiv*14*sara))]

		for ch in channels:
			tStart = time.time()

			# get trace status, skip if OFF
			trc = self.query("c"+str(ch)+":trace?").decode().strip()
			if trc == 'OFF':
				print("\tC%d: OFF" % ch )
				continue

			# get volts per div, offset...
			vdivStr = self.query("c"+str(ch)+":vdiv?")
			ofstStr = self.query("c"+str(ch)+":ofst?")
			vdiv = float(vdivStr)
			ofst = float(ofstStr)

			# now to get waveform data...
			self.write("c"+str(ch)+":wf? dat2")
			self._socket.settimeout(3000)
			header = self._socket.recv(16).decode('ascii')
			blockLen = int(header[-9:])
			wfData = bytes()
			bufsize = 4096
			while len(wfData) < (blockLen - bufsize):
				wfData += self._socket.recv(bufsize)
				time.sleep(0.005)
			wfData += self._socket.recv(blockLen-len(wfData))
			self._socket.recv(2)
			points = len(wfData)

			# create waveform object
			chData = self._ChannelData()
			chData.vdiv = vdiv
			chData.ofst = ofst
			chData.volts = []


			# convert to actual voltage / time readings

			for i in range(points):
				val = wfData[i]
				if val > 127: val -= 256
				chData.volts.append(val * vdiv/25 - ofst)

			# store min/ max values for easier plotting...
			chData.vmin = min(chData.volts)
			chData.vmax = max(chData.volts)

			# place channeldata obj in waveform obj...
			setattr(wfs, "c"+str(ch), chData)
			self._socket.settimeout(self._timeout)

			# Print time this all took to console for fun.
			elapsed = time.time() - tStart
			print("\tC%d: %d points in %.2f secs" % (ch, points, elapsed) )

		self.write("chdr short")

		return wfs
